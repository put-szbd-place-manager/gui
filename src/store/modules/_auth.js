/* eslint-disable no-param-reassign */
import api from '../../plugins/api';

const authModule = {
  namespaced: true,

  state: {
    token: localStorage.getItem('token') || '',
    user: (localStorage.getItem('user') && JSON.parse(localStorage.getItem('user'))) || {},
  },

  mutations: {
    setToken(state, { token }) {
      state.token = token;
      localStorage.setItem('token', token);
      api.defaults.headers.common.Authorization = `BEARER ${token}`;
    },

    setUserData(state, { user }) {
      state.user = user;
      localStorage.setItem('user', JSON.stringify(user));
    },

    purgeAuthData(state) {
      state.token = '';
      state.user = {};
      localStorage.removeItem('token');
      localStorage.removeItem('user');
    },
  },

  actions: {
    async login({ commit }, loginInfo) {
      const { data } = await api.post('user/login', loginInfo);
      commit('setToken', data);
      commit('setUserData', data);
    },

    async register({ commit }, registerInfo) {
      const { data } = await api.post('user/', registerInfo);
      commit('setUserData', data);
    },

    async update({ commit }, userData) {
      const { data } = await api.put('user', userData);
      commit('setUserData', { user: data });
    },

    async delete({ commit }) {
      await api.delete('user');
      commit('purgeAuthData');
    },
  },
};

export default authModule;
