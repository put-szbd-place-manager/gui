import _apiGenerator from '../_apiGenerator';

const placesModule = {
  ..._apiGenerator('places'),
};

export default placesModule;
