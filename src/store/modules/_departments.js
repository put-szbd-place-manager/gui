import _apiGenerator from '../_apiGenerator';

const departmentsModule = {
  ..._apiGenerator('departments'),
};

export default departmentsModule;
