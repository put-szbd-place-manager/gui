import _apiGenerator from '../_apiGenerator';

const contactsModule = {
  ..._apiGenerator('contacts'),
};

export default contactsModule;
