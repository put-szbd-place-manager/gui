import _apiGenerator from '../_apiGenerator';

const customOpeningHoursModule = {
  ..._apiGenerator('customOpeningHours'),
};

export default customOpeningHoursModule;
