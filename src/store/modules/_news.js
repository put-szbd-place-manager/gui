import _apiGenerator from '../_apiGenerator';

const newsModule = {
  ..._apiGenerator('news'),
};

export default newsModule;
