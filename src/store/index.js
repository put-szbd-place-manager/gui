import Vue from 'vue';
import Vuex from 'vuex';
import authModule from './modules/_auth';
import placesModule from './modules/_places';
import contactsModule from './modules/_contacts';
import departmentsModule from './modules/_departments';
import customOpeningHoursModule from './modules/_customOpeningHours';
import newsModule from './modules/_news';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth: authModule,
    places: placesModule,
    contacts: contactsModule,
    departments: departmentsModule,
    customOpeningHours: customOpeningHoursModule,
    news: newsModule,
  },
});
