/* eslint-disable no-param-reassign */
import api from '../plugins/api';

// eslint-disable-next-line arrow-parens
export default (name) => ({
  namespaced: true,

  state: {
    [`${name}All`]: [],
    [`${name}Single`]: {},
  },

  mutations: {
    setAll(state, data) {
      state[`${name}All`] = data;
    },

    setOne(state, data) {
      state[`${name}Single`] = data;
    },
  },

  actions: {
    async findAll({ commit }, payload) {
      const { data } = await api.get(name, payload);
      commit('setAll', data);
    },

    async find({ commit }, id) {
      const { data } = await api.get(`${name}/${id}`);
      commit('setOne', data);
    },

    async create(context, payload) {
      await api.post(name, payload);
    },

    async update(context, payload) {
      await api.put(`${name}/${payload.id}`, payload);
    },

    async delete(context, id) {
      await api.delete(`${name}/${id}`);
    },
  },
});
