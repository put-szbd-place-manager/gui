import Vue from 'vue';
import Vuelidate from 'vuelidate';
import { helpers } from 'vuelidate/lib/validators';

const moment = require('moment');

Vue.use(Vuelidate);

// eslint-disable-next-line import/prefer-default-export
export const isBeforeCloses = prop => helpers.withParams({ prop }, (value, parentVm) => {
  const closed = helpers.ref('closed', this, parentVm);
  const closes = helpers.ref(prop, this, parentVm);

  if (closed) {
    return true;
  }
  if (value && closes) {
    const closes_at = moment(closes, 'HH:mm');
    const opens_at = moment(value, 'HH:mm');
    return opens_at.isBefore(closes_at);
  }
  return false;
});
