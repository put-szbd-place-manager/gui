import Vue from 'vue';
import Swal from 'sweetalert2';
import './plugins/vuetify';
import './plugins/vuelidate';
import App from './App.vue';
import router from './router/index';
import store from './store/index';
import api from './plugins/api';

Vue.config.productionTip = false;

if (store.state.auth.token) {
  api.defaults.headers.common.Authorization = `BEARER ${store.state.auth.token}`;
}

router.beforeEach((to, from, next) => {
  if (from.name === 'undefined') {
    next();
    return;
  }
  if (to.name === 'login' || to.name === 'register') {
    next();
    return;
  }
  if (!api.defaults.headers.common.Authorization) {
    Swal({
      type: 'error',
      title: 'Authorization error',
      text: 'Unauthorized access',
    });
    next({ name: 'login' });
    return;
  }
  next();
});

api.interceptors.response.use(
  response => response,
  (error) => {
    const errorResponse = error.response;
    if (errorResponse.status === 401) {
      Swal({
        type: 'error',
        title: 'Authorization error',
        text: 'You have been logged off',
      });
      router.replace({ name: 'login' });
    }
    return Promise.reject(error);
  },
);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
