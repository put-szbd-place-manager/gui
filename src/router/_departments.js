import RouterOutlet from '@/components/RouterOutlet/RouterOutlet.vue';
import DepartmentsIndex from '@/views/Departments/index.vue';
import DepartmentsEdit from '@/views/Departments/edit.vue';
import DepartmentsCreate from '@/views/Departments/create.vue';

export default [
  {
    path: 'departments',
    component: RouterOutlet,
    children: [
      {
        path: '',
        name: 'departments.index',
        component: DepartmentsIndex,
      },
      {
        path: 'create',
        name: 'departments.create',
        component: DepartmentsCreate,
      },
      {
        path: ':id',
        name: 'departments.edit',
        component: DepartmentsEdit,
      },
    ],
  },
];
