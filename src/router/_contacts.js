import RouterOutlet from '@/components/RouterOutlet/RouterOutlet.vue';
import ContactsIndex from '@/views/Contacts/index.vue';
import ContactsEdit from '@/views/Contacts/edit.vue';
import ContactsCreate from '@/views/Contacts/create.vue';

export default [
  {
    path: 'contacts',
    component: RouterOutlet,
    children: [
      {
        path: '',
        name: 'contacts.index',
        component: ContactsIndex,
      },
      {
        path: 'create',
        name: 'contacts.create',
        component: ContactsCreate,
      },
      {
        path: ':id',
        name: 'contacts.edit',
        component: ContactsEdit,
      },
    ],
  },
];
