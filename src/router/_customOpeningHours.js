import RouterOutlet from '@/components/RouterOutlet/RouterOutlet.vue';
import CustomOpeningHoursIndex from '@/views/CustomOpeningHours/index.vue';
import CustomOpeningHoursEdit from '@/views/CustomOpeningHours/edit.vue';
import CustomOpeningHoursCreate from '@/views/CustomOpeningHours/create.vue';

export default [
  {
    path: 'customOpeningHours',
    component: RouterOutlet,
    children: [
      {
        path: '',
        name: 'customOpeningHours.index',
        component: CustomOpeningHoursIndex,
      },
      {
        path: 'create',
        name: 'customOpeningHours.create',
        component: CustomOpeningHoursCreate,
      },
      {
        path: ':id',
        name: 'customOpeningHours.edit',
        component: CustomOpeningHoursEdit,
      },
    ],
  },
];
