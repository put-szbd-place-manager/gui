import RouterOutlet from '@/components/RouterOutlet/RouterOutlet.vue';
import PlacesIndex from '@/views/Places/index.vue';
import PlacesEdit from '@/views/Places/edit.vue';
import PlacesCreate from '@/views/Places/create.vue';

export default [
  {
    path: 'places',
    component: RouterOutlet,
    children: [
      {
        path: '',
        name: 'places.index',
        component: PlacesIndex,
      },
      {
        path: 'create',
        name: 'places.create',
        component: PlacesCreate,
      },
      {
        path: ':id',
        name: 'places.edit',
        component: PlacesEdit,
      },
    ],
  },
];
