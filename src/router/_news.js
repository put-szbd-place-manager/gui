import RouterOutlet from '@/components/RouterOutlet/RouterOutlet.vue';
import NewsIndex from '@/views/News/index.vue';
import NewsEdit from '@/views/News/edit.vue';
import NewsCreate from '@/views/News/create.vue';

export default [
  {
    path: 'news',
    component: RouterOutlet,
    children: [
      {
        path: '',
        name: 'news.index',
        component: NewsIndex,
      },
      {
        path: 'create',
        name: 'news.create',
        component: NewsCreate,
      },
      {
        path: ':id',
        name: 'news.edit',
        component: NewsEdit,
      },
    ],
  },
];
