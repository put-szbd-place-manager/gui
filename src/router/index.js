import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/components/Layout/Layout.vue';
import Home from '@/views/Home.vue';
import Login from '@/views/Auth/Login.vue';
import Register from '@/views/Auth/Register.vue';
import User from '@/views/User/index.vue';

import placesRoutes from './_places';
import contactsRoutes from './_contacts';
import departmentsRoutes from './_departments';
import customOpeningHoursRoutes from './_customOpeningHours';
import newsRoutes from './_news';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: 'login',
      name: 'login',
      component: Login,
    },
    {
      path: 'register',
      name: 'register',
      component: Register,
    },
    {
      path: '/',
      component: Layout,
      children: [
        {
          path: '',
          name: 'homepage',
          component: Home,
          redirect: 'places/',
        },
        ...placesRoutes,
        ...contactsRoutes,
        ...departmentsRoutes,
        ...customOpeningHoursRoutes,
        ...newsRoutes,
        {
          path: 'user',
          name: 'user',
          component: User,
        },
        {
          path: '*',
          redirect: 'places/',
        },
      ],
    },
  ],
});
